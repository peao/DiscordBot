'use strict'

const crypto = require('crypto')

const command = 'rps'
const usage = 'rps [(rock|r)|(paper|p)|(scissors|s)]'
const description = 'Rock paper scissors game.'

let data = {}

function winner(p1, p2) {
  let result =-1
  if (p1 === p2) result = -1
  // P1 is rock
  else if (p1 === 'r') {
    if (p2 == 'p') result = 1
    else result = 0
  } 
  // P1 is paper
  else if (p1 === 'p') {
    if (p2 == 'r') result = 0
    else result = 1
  }
  // P1 is scissors
  else {
    if (p2 == 'r') result = 1
    else result = 0
  }
  return result
}

function aiDecision (playerId) {
  const max = data[playerId].reduce((a,b) => a + b, 3)
  const randomNr = Math.floor(Math.random() * max)
  const rMinRange = 0
  const rMaxRange = data[playerId][2]
  const pMinRange = rMaxRange + 1
  const pMaxRange = pMinRange + data[playerId][0]
  const sMinRange = pMaxRange + 1
  const sMaxRange = sMinRange + data[playerId][1]
  let decision

  if (randomNr >= rMinRange && randomNr <= rMaxRange) decision = 'r'
  else if (randomNr >= pMinRange && randomNr <= pMaxRange) decision = 'p'
  else if (randomNr >= sMinRange && randomNr <= sMaxRange) decision = 's'
  else throw new Error('not a valid random number')
  
  return decision
}

const action = (message, args) => {
  if (args.length > 1) return message.channel.send(`Invalid usage!\nUsage: ${usage}`)
  if (!data[message.author.id]) data[message.author.id] = [0,0,0]
  
  let outcome
  let response = 'You: '
  let aiChoise = aiDecision(message.author.id)
  
  switch(args[0]) {
    case 'rock':
    case 'r':
      data[message.author.id][0]++
      response += `r\tAI: ${aiChoise}`
      outcome = winner('r', aiChoise)
      break
    case 'paper':
    case 'p':
    data[message.author.id][1]++
      response += `p\tAI: ${aiChoise}`
      outcome = winner('p', aiChoise)
      break
    case 'scissors':
    case 's':
    data[message.author.id][2]++
      response += `s\tAI: ${aiChoise}`
      outcome = winner('s', aiChoise)
      break
    default:
      return message.channel.send(`Invalid argument: ${args[0]}!\nUsage: ${usage}`)
  }

  switch (outcome) {
    case -1:
      response += '\tResult: TIE!'
      break
    case 0:
      response += '\tResult: YOU WIN!'
      break
    case 1:
    response += '\tResult: YOU LOOSE!'
      break
    default:
      break
  }
  return message.channel.send(response)
}

module.exports = () => ({
  command,
  action,
  description,
  usage
})