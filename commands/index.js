'use strict'

const fs = require('fs')
const path = require('path')
const ownFilename = path.basename(__filename);

const validate = (c) => {
    if (!c) return false
    if (typeof c !== 'object') return false
    if (!c.command && typeof c.command !== 'string') return false
    if (!c.action && typeof c.action !== 'function') return false
    if (!c.description && typeof c.description !== 'string') return false
    if (!c.usage && typeof c.usage !== 'string') return false
    return true
}

module.exports = client => {
    let commands = {}
    
    fs.readdirSync(__dirname).forEach(file => {
        if(file === ownFilename) return
        const c = require('./' + file)(client)
        
        if (!validate(c)) return console.log(`Could not load command from file: "${file}".`)
        
        console.log(`Command: "${c.command}" from file: "${file}" was loaded`)
        commands[c.command] = {
            action: c.action,
            description: c.description,
            usage: c.usage
        }
    })

    return commands
}
