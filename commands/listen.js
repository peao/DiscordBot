'use strict'

const command = 'listen'
const description = 'listen description'
const usage = 'listen usage'
const id = '139085587243073536'
let connection = undefined
let recevier = undefined

const action = (client) => (message, args) => {
  if (message.author.id !== id) return message.channel.send(`You do not have permission to use command: ${command}`)
  
  switch(args[0]){
    case 'join':
      joinChannel(message)
      break
    case 'leave':
      leaveChannel()
      break
    case 'start':
      startRecording()
      break
    case 'end':
      break
    default:
      message.channel.send(`Invalid argument(s).\n${usage}`)
  }
}

function joinChannel(message) {
  if (!message.member.voiceChannel) return message.channel.send(`You need to join a voiceChannel`)
  return message.member.voiceChannel.join()
    .then(con => {
      console.log(`Connected to voiceChannel: ${message.member.voiceChannel.name}`)
      connection = con
    })
    .catch(err => console.log(err))
}

function leaveChannel() {
  if (!connection) throw new Error('connection is undefined')
  
  connection.channel.leave()
  console.log(`Left voiceChannel: ${connection.channel.name}`)
  connection = undefined
}

function startRecording() {
  if (!connection) throw new Error('connection is undefined')
  console.log('Start recording')
  recevier = connection.createReceiver()
  recevier.on('opus', (user, data) => {
    const hexString = data.toString('hex')
    console.log(`User: ${user}\nData: ${data}`)
  })
}

module.exports = (client) => ({
  command,
  action: action(client),
  description,
  usage
})