'use strict'

module.exports = () => ({
    command: 'ping',
    action: (message) => { 
        message.channel.send('pong')
    },
    description: 'Simple command that responds with pong.',
    usage: 'ping'
})