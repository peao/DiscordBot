'use strict'

require('dotenv').config()
const initBot = require('./bot')
const token = process.env.DISCORD_TOKEN

initBot()
  .then(bot => bot.login(token))
  .catch(err => {
    console.error(err)
    process.exit()
  })
