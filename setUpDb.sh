#!/bin/bash

DB_DIR_PATH=/srv/mongodb
DB_LOG_DIR_PATH=/var/log
DB_LOG_PATH=/var/log/mongod.log

sudo mkdir -p "$DB_DIR_PATH"
sudo mkdir -p "$DB_LOG_DIR_PATH"
sudo mongod --fork --dbpath "$DB_DIR_PATH" --logpath "$DB_LOG_PATH"