'use strict'

const mongoClient = require('mongodb').MongoClient
const host = process.env.DB_HOST
const dbName = process.env.DB_NAME
const url = 'mongodb://' + host

function insertMember(member) {
  return mongoClient.connect(url)
    .then(db => {
      return db.db(dbName).collection('members').insertOne(member, (err, res) => {
        if (err) return Promise.reject(err)
        db.close()
        return Promise.resolve(res)
      })
    })
}

function insertMultipleMembers(members) {
  return mongoClient.connect(url)
    .then(db => {
      return db.db(dbName).collection('members').insertMany(members, (err, res) => {
        if (err) return Promise.reject(err)
        db.close()
        return Promise.resolve(res)
      })
    })
}

function getMembers() {
  return mongoClient.connect(url)
    .then(db => {
      return db.db(dbName).collection('members').find({}).toArray()
        .then(res => {
          if (err) return Promise.reject(err)
          db.close()
          return Promise.resolve(res)
        })
    })
}

module.exports = ({
  insertMember,
  insertMultipleMembers,
  getMembers
})