'use strict'

const Discord = require('discord.js')
const client = new Discord.Client()
const commands = require('./commands')(client)
const dbFunction = require('./database/db')

const prefix = process.env.PREFIX
const contentMax = 255
let members

client.on('ready', () => {
    console.log('Bot is ready!')
})

client.on('disconnect', () => {
  console.log('Bot disconnected!')
})

client.on('message', message => {
  if (message.author.bot) return
  if (message.content[0] !== prefix) return
  if (message.content.length > 255) return message.channel.send(`Invalid content length ${message.content.length}. Max content length is 255.`)

  const args = message.content.substr(1).split(' ')
  const command = args.shift().toLowerCase()

  if (!commands[command]) {
      return message.channel.send(`Could not find command: "${command}"`)
  }

  commands[command].action(message, args)
})

module.exports = () => new Promise((resolve) => resolve(client)) /* dbFunction.getMembers()
  .then(result => {
    members = result
    console.log(members)
    return Promise.resolve(client)
  }) */
